#!/usr/bin/env python3
# staticsitegen
# Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
Create some simple template stuff. Just point to a directory and load templates.
TEMPLATE FORMAT:
Templates are in files of the form "<name>.template"
Note that when loading templates, the filesystem is essentially flattened (for now)
so two templates with the same name does not make much sense and will probably 
cause weirdness).

Templates have a fairly simple format. The first lines are a set of pairs of the form:
    <parameter-name>:<parameter-type>
    where parameter-type for now is one of:
        * raw ~ raw html/text
        * markdown ~ markdown as formatted by mistune - this means that the input will be
          processed as markdown into html before being passed.
    Optionally with a postfix of [] to indicate a list. These parameters are ordered.

This section is terminated by a line containing only the text "done" and whitespace. all other invalid lines
before this are ignored (if they start with # they are also ignored).

After this, the template is a bunch of html which is a python format string - with one exception.
You can specify commands using $<command> as a single line (stripped) with parameter specifications as follows:
    $command:args...
    where args is a comma separated list of names to pass as arguments, with optional creation of tuples 
    using () and references to things other than parameters - in particular, template names usually - provided
    using "&<name of not parameter>". you can also use {<new name>:<curr name> | <new_name>:<curr_name[index]>,...} 
    to define a parameter name mapping, bearing in mind that references do not work inside the mapping. Indexing
    can be done on list/tuple params.

    To mark the end of the command you simply have on a single line a single $ with anything after - or the command 
    itself can end with $ which provides an empty command text value. In effect commands are started with $ 
    and ended with $, but only with single line commands. For multiline commands, you terminate with 
    $~

    Commands CAN be put inside other commands with no necessary extra delimiting, but only in multiline commands
    (otherwise the processer will mistake the start of the inner command for a terminating $)


COMMANDS:
    * $template:&name,{new parameter names: curr parameters...}:
        Generate from the given template and inject the output straight into this template.
    * $foreach:[list-params-to-zip...], {new_names:other_params...}:
        Iterate over all the list parameters provided zipped together, instantiating the contained template text
        with the zipped tuple provided as the keyword argument to the format string "iter_param"
        Note that the creation of complex datastructures is possible if desired!: 
            * [] creates a list of parameter-only types - this means no references or mappings
            * () creates a tuple of parameters and references - but no mappings
            * {} this is just a mapping. no complex stuff here sadly (for now anyway)
    * The special empty command created by $$ acts to delimit back to $
"""
from typing import *
import string

valid_param_types = {"raw", "markdown"}


def pull_parameters(text: str) -> Tuple[Dict[str, Tuple[str, bool]], str]:
    """
    Gets the parameters out of the template text above, returning a pair:
      * a dictionary mapping parameter names to a pair of (type, is list/tuple)
      * the rest of the template string.
    """

    spliced = text.splitlines()
    i = 0
    while i < len(spliced) and spliced[i].strip() != "done":
        i += 1
    # we are now at the line "done"
    # or nothing showed up at all.
    parameter_lines = spliced[:i] 
    template_lines = []
    if i < len(spliced) - 1:  # We are not on the last line or line before
        template_lines = spliced[i+1:]

    # Now parse the parameter specifications.
    parameters = {}
    for _line in parameter_lines:
        line = _line.strip()
        if not line.startswith('#'):  # comments
            # length 1 or 2 - 1 = invalid, 2 = we have pair
            components = line.split(':', 1)
            if len(components) == 1:
                break

            components[0] = components[0].strip()
            components[1] = components[1].strip()
            is_listtype = False
            # check for arraytype and pull the last 2 chars off.
            if components[1].endswith("[]"):
                is_listtype = True
                components[1] = components[1][:-2]
            # add to params if valid type, else error out
            if components[1] in valid_param_types:
                parameters[components[0]] = (components[1], is_listtype)
            else:
                raise Exception("invalid template parameter type: {}".format(components[1]))

    return parameters, '\n'.join(template_lines)


def scan_for_next_command(text: str, startidx: int) -> Tuple[Optional[Tuple[str, str, int]], int] :
    """
    Spits out a command string from the text if it exists.
    This returns the following:
        * An optional tuple of the found command containing (command+args string, block string, start index (including $)
        * where we scanned up to. if a command was found, this is one-after the terminating $~. If it wasn't found, 
          then this is len(text) because we scanned all the way to the end.
    """
    if startidx >= len(text):
        return None, len(text)
    i = startidx
    command_depth = 0  # when > 0 we are in command, else outside command.

    SINGLE_LINE_MODE = "single_line_cmd"
    MULTILINE_MODE = "multiline_cmd"  # selected if we find a \n before terminating $$
    mode = SINGLE_LINE_MODE
    
    # find start of command.
    while i < len(text) - 1 and (text[i] != '$' or text[i+1] in set(string.whitespace) | {'~'}):
        i += 1

    # if we are one-after end or exactly at end, return None, i:
    if i >= len(text) - 1:
        return None, i
    
    command_start = i
    # we are in a command. Attempt to locate either a \n (to enter multiline mode) or $ (single/inline mode).
    command_depth = 1
    # note we have to move forward one just to avoid immediately triggering on the current one.
    i += 1 
    while i < len(text) and not text[i] in {'\n', '$'}:
        i += 1

    # i is now at either the \n or $ terminating or continuing the command.

    def check_not_end_looking_for_matching(i):
        if i >= len(text):
            raise Exception("Could not find matching $ for command starting at {!s}, in template:\n{}\n".format(command_start, text))
    check_not_end_looking_for_matching(i)
    
    # command string is per-line thingy so we can grab it now ^.^
    # command_start: includes the initial $, which we want gone.
    command_string = text[command_start + 1 : i].strip()
    command_string_end = i

    # single line mode...
    if text[i] == '$':
        # we always need to take one-after the character after the command string - i is currently
        # AT '$', we need ONE AFTER.
        return (command_string, "", command_start), i + 1
    mode = MULTILINE_MODE
    # now we must be on a newline and into multiline mode, and scan for our end :)
    while i < len(text) and command_depth > 0:
        while i < len(text) and text[i] != '$':
            i += 1
        i += 1
        # one past the $ found.
        check_not_end_looking_for_matching(i)
        if text[i] == "~":  #termination command for sure, and of a multiliner at that, so dump out.
            command_depth -= 1
            i += 1
            continue


        # at either the termination command or another command or end. If another command we check if 
        # inline and immediately process it so it does not cause further issues. 
        while i < len(text) and text[i] not in {'\n', '$'}:
            i += 1
        check_not_end_looking_for_matching(i)
        if text[i] == '$':  # inline since we found $ before newline. Add one and move on.
            i+= 1
            continue
        # newline, so we have found a multiline command, which we shall enter!
        else:
            i+=1
            command_depth += 1
    if command_depth > 0:
        # guarunteed to give the error we want.
        check_not_end_looking_for_matching(i)

    # i is now one-after the ~ of the terminating command.
    # we can get the text block now! >> note we do not include the \n of the multiline
    # command in the text block
    text_block = text[command_string_end + 1: i - 2]
    return (command_string, text_block, command_start), i


def chunk_out_command_data(template_text: str) -> List[Union[str, Tuple[str, str]]]:
    """
    This takes template text, and produces a list containing strings - for actual template data
    - and tuples - which are pairs of (command string, command text block) - for commands.

    Note that these are NOT necessarily separate lines! do not assume they join that way, 
    just join them directly :p.
    """

    # Continue pulling commands until we are very very done ^.^
    # Commands here are tuple of (cmd, text block, full start idx, one-after full end idx)

    
    intermediate_list = []
    
    cmdtuple, curr_idx = scan_for_next_command(template_text, 0)
    while cmdtuple is not None:
        command_string, text_block, start_idx = cmdtuple
        intermediate_list.append((command_string, text_block, start_idx, curr_idx))
        cmdtuple, curr_idx = scan_for_next_command(template_text, curr_idx)

    # Now we have all the regions containing commands in order of start/end idx
    # time to expand out
    # each iteration adds the text section before it, too.
    combined_list = []
    for i in range(len(intermediate_list)):
        previous_end_idx = intermediate_list[i-1][3] if i > 0 else 0
        # add prev text
        combined_list.append(template_text[previous_end_idx: intermediate_list[i][2]])
        # add ourselves
        combined_list.append((intermediate_list[i][0], intermediate_list[i][1]))

    # add the end:
    if len(intermediate_list) == 0:
        combined_list.append(template_text)
    else:
        combined_list.append(template_text[intermediate_list[-1][3]:-1])
    # remove empty strings
    combined_list = [f for f in combined_list if f != ""]
    return combined_list    


class Arg(object):
    """
    Simple trivial baseclass for arguments passed to a command.
    Subclasses for this essentially act as things you can extract
    a data structure from given a general environment context.

    ENVIRONMENT CONTEXT:
        * The environment context is a map of text data structures, essentially.
          data values are simply keys in this structure, and they should all
          map down to text/html eventually 
        * Params act as references to this data, or as conglomerations of it,
          and can be used to get hold of relevant data structures.
    """

class ArgParam(Arg):
    """
    An argument that is just a parameter itself.
    """
    def __init__(self, parameter_name: str):
        self._parameter_name = parameter_name

    def get_data_structure(self, environ: dict) -> Any:
        """
        This method is used to get data structures from the environment...
        """
        return environ[self._parameter_name]

class ArgRef(Arg):
    """
    An argument that acts as a reference to some non-param
    thing (e.g. a template)
    """
    def __init__(self, referenced_object: str):
        self._referenced_object = referenced_object

class ArgMapping(Arg):
    """
    Holds name->argument mapping. Note that this does not work with
    ArgRefs, only ArgParams
    """
    def __init__(self, mapping: Dict[str, ArgParam]):
        super().__init__()
        self._mapping = mapping

    def create_new_environment(self, environment: Dict[str, Any]) -> Dict[str, Any]:
        """
        Uses the provided mappings to create a new environment with appropriate datastructures!
        """
        res = dict()
        for k in self._mapping.keys():
            res[k] = self._mapping[k].get_data_structure()
        return res


class MultiArgTuple(Arg):
    """
    Holds a list of data arguments and combines them into a
    new unnamed argument...
    """
    def __init__(self, contained_args: Iterable[Arg]):
        self._arguments = contained_args

    def get_values(self) -> Tuple:
        """
        Gets the raw Arg instances in this tuple to be processed appropriately.
        """
        return self._arguments

class MultiParamList(MultiArgTuple, ArgParam):
    """
    A monotype list of arguments that cannot contain references or mappings.
    """
    def __init__(self, contained_args: Iterable[ArgParam]):
        super(MultiArgTuple, self).__init__(contained_args)

    def get_data_structure(self, environ: dict) -> Any:
        return [x.get_data_structure() for x in self._arguments]


class ArgIndexedParam(ArgParam):
    """
    An argument that is an indexed version of a list-type
    parameter.
    """
    def __init__(self, param: ArgParam, idx: int):
        self._param = param
        self._idx = idx

    def get_data_structure(self, environ: dict) -> Any:
        return self._param.get_data_structure()[self._idx]



def merge_dictionaries(*dicts: dict) -> dict:
    """
    Merge dictionaries in priority order - i.e. each new dict will
    override keys in the older dicts that are the same with their own
    values.
    """
    result = {}
    for d in dicts:
        result.update(d)

    return result



def tokenise_argstring(argstring: str) -> List[str]:
    """
    Split an argument string into tokens - specifically:
    &, strings, [, (, {, }, ), ] tokens, etc. "" create strings.
    backslashes are delimiters - also does the delimiting.
    """
    def parse_delim(char: str) -> str:
        """
        Parses simple delimiter chars
        """
        mapping = {
            '"': '"',
            'n': '\n',
            't': '\t',
            '\\': '\\'
        }
        return char if char not in mapping.keys() else mapping[char]

    state = {}
    result = [""]

    def ensure_new_token(contents: str=""):
        """
        Creates a new token if there is no empty one available.
        """
        if len(result) == 0:
            result.append(contents)
        elif result[-1] == "":
            result[-1] = contents
        else:
            result.append(contents)

    def tok_append(content: str):
        """
        Appends to the current token the given data
        """
        if len(result) == 0:
            result.append("")
            
        result[-1] = result[-1] + content

    for i in range(len(argstring)):
        char = argstring[i]
        # state tags to add/remove
        to_remove_tags = []
        to_add_tags = []


        if "delim" in state and "string" in state:
            tok_append(parse_delim(char))
            to_remove_tags.append("delim")
        # for all of these we can assume no delims
        elif char in {"'", "\""}:
            # string parsing
            if "string" in state and char == state["curr_string_char"]:
                tok_append(char)  # glue this onto the end
                ensure_new_token()
                to_remove_tags.append("string")

            elif "string" in state and char != state["curr_string_char"]:
                tok_append(char)

            elif "string" not in state:
                to_add_tags.append("string")
                state["curr_string_char"] = char
                ensure_new_token(char)

        elif char == "\\" and "string" in state:
                to_add_tags.append("delim")
        # We parsed all characters that could interfere with a string.
        elif "string" in state:
            tok_append(char)
        elif char == "&":  # references.
            ensure_new_token(char)
            ensure_new_token()
        elif char in string.whitespace:
            ensure_new_token()
        elif char in {'[', '(', '{', '}', ')', ']', ':', ','}:  # bracket tokens...
            ensure_new_token(char)
            ensure_new_token()
        else:
            tok_append(char)

        for k in to_remove_tags:
            if k in state:
                del state[k]
        for k in to_add_tags:
            if k not in state:
                state[k] = True


        # remove the last if it is an empty token :) 
    return result if result[-1] != "" else result[:-1]

def categorise_tokens(tokens: Iterable[str]) -> Iterable[Tuple[str, Optional[Union[str, int]]]]:
    """
    Categorise and process tokens into various types. Returns an iterable
    of pairs of (type, value), though the value may well be meaningless or None.
    
    Categories:
        * reference - value is None, indicates a simple &
        * string - value is the actual value of the string. In correct syntax, this should really only come after
          reference (and maybe in future as a value in place of a parameter name
        * name - a name of some kind. value is the name. This is stuff without quotes, so after a reference 
          there could be a name or string.
        * [, (, { - each of these categories indicates the start of that type of 
          bracket. value is none.
        * ], ), } - each of these categories indicates the end of that type
          of bracket. Value is none
        * : for the colons. simply a divider.
        * statement - for commas. Value is None.
        * int - for 0123456789 etc. value is the number in question.
    Note that tokens should be empty.
    """
    def transform(d: str) -> Tuple[str, Optional[str]]:
        if d == '&':
            return ("reference", None)
        elif d in {'[', '{', '(', ')', '}', ']'}:
            return (d, None)
        elif d[0] in {'"', "'"}:
            char = d[0]
            if d[-1] != char:
                raise Exception("Error: no matching final {} found for string token {}".format(char, d))
            else:
                return ("string", d[1:-1])
        elif d == ',':
            return ("statement", None)
        elif d[0] in "+-" + string.digits and all(a in string.digits for a in d[1:]) :
            return ("int", int(d))
        elif d[0] not in (string.digits + string.punctuation + "+-"):
            return ("name", d)
        else:
            raise Exception("Unrecognised token: {}".format(d))
    
    return (transform(d) for d in tokens)


def create_ast(tokens: Iterable[Tuple[str, Optional[Union[str, int]]]]) -> List[Tuple[str, dict]]:
    """
    This takes a processed token list as from categorise_tokens
    and turns it into a parsed AST/list of parameter-structures of the
    parameters
    """
    Operation = Tuple[str, Callable, Optional[List[int]]]

    Transition = Tuple[
            List[str],
            Optional[Union[str, int]],
            Optional[Operation],
            bool
    ]

    def op(op_name: str, transform: Callable, extra_state_idxs: Optional[List[int]]=None) -> Operation:
        """
        Create an operation as defined below.
        """
        extra_state_idxs = extra_state_idxs if extra_state_idxs is not None else []
        return (op_name, transform, extra_state_idxs)

    StateStackPattern = List[Optional[Set[str]]]]

    def state_stack_pattern(*args: Optional[Set[str]]):
        """
        Create a pattern (to be used with associated operation)
        as specified below
        """
        return [*args]
    
    def pattern_op(_patterns: List[Tuple[StateStackPattern, Operation]]) -> Operation:
        """
        Creates a dynamic operation that *pattern matches* upon the current stack state.
        
        In particular:
            * Each matcher is a pair (pattern, operation)
            * pattern is a list of optional sets. Each of these sets is a set of allowed values for the
              the given stack depth - working BACKWARDs from the end of the pattern the lower in the stack
              you go - for instance [{"a", "b"}, {"c"}] matches when the state stack has "c" as one 
              above the current state, and a or b as two above the current state:
               * ["3", "a", "c"] would match it
               * ["5", "2", "b", "c"] would match it
               * ["c"] would not match it
            * making one of the elements of the list None instead of a set 
              acts as a wildcard to match it to anything at that position.

        Note that if a state does not match any pattern, then it is an 
        error. to do nothing instead of erroring, add a ([], None) pattern/op
        since that will match everything and produce a nullop.

        Patterns are evaluated in order of being provided i.e. the first
        matching pattern is used.
        """

        # Fills all the patterns up with wildcards lower in the stack so they
        # are all the same length :3
        max_pattern_length = max(len(a[0]) for a in _patterns)
        normalised_patterns = [
            (
                # add wildcards to the pattern to fill it up
                [None] * (max_pattern_length - len(pat[0])) + pat[0],
                # the operation
                pat[1]
            ) for pat in _patterns
        ]

        state_depth_idxs = [i for i in range(max_pattern_length, 0, -1)] # stop nonincl

        def matches(pattern, states):  # assumes same length...
            element_matches = (
                (states[i] in pattern[i] if pattern[i] is not None else True)
                for i in range(len(pattern))
            )
            return all(element_matches)
        
        def operator_function(token, *provided_state):
            """
            The actual operator

            Note that the provided state contains all elements of the
            state down to the max depth, but in reverse order (deepest
            first) so it matches the patterns :3
            """
            for pair_idx in range(len(normalised_patterns)):
                if matches(normalised_patterns[pair_idx][0], provided_state):
                    return normalised_patterns[pair_idx][1]
            raise Exception("Unrecognised parser state: {!s}".format(provided_state))

        return op("dyn", operator_function, state_depth_idxs)

    def state_transition(
        stack_glues: List[str], 
        change_curr_state_to: Optional[Union[str, int]]=0, 
        operation: Optional[Operation]=None,
        rescan_after: bool=False
    ):
        """
        Creates a state transition entry.

        First entry is the states to push to the top of the stack.

        Second entry is str or int. If int, then we pop the given number of state entries off the stack.
        If str, then this indicates that the current stack state should be changed to that.
        This occurs before any things from argument one are added on top of the stack.
        If int, then we pop 

        Operation is a tuple of (str, callable, Optional[List[int]]) and dictates what should be done to the result stack.
        Whatever is returned from the callable if not None, is appended to the result stack.

        Before any of these arguments are passed, the current categorised token that triggered the call
        is passed as the first argument nya ^.^. If the last element is not None, then each state above the
        current by the given amount is passed as an argument. e.g. [1, 2] would get the state above the
        current one in the stack and two above the current one, each passed as an argument to the
        operation. Note that if these states go out of stack range, the value passed is None

        in particular:
            The first argument being "generate" means that no arguments are passed (other than the categorised token ^.^)

            the first argument being "pop" means that the last element of the result stack is passed to operation and
            removed from the stack.

            the first argument being "merge" means that the top two elements of the stack are passed to operation, and
            both are removed from the result stack (return the first if you want it to be readded)

            the first argument being "merge_pair" is like above but 3 elements instead of two.

            the first argument being "dyn" means that the same protocol as "generate" is used except the
            return argument is interpreted as another operation, which is then performed. Returning None
            means no operation is applied (as expected)

        The final element is a boolean that if true, indicates that the token should get rescanned
        """
        return (stack_glues, change_curr_state_to, operation, rescan_after)




    states = ["new_provided_argument"]  # this is a stack.
    results = []  # this is also a stack containing arguments and intermediate results.

    def apply_state_transition(token: Tuple[str, Optional[int, str]], transition: Transition):
        """
        Actually apply the state transition to the current set of results. Works as specified above.
        """
        # removal mode
        if isinstance(transition[1], int) or transition[1] is None:
            pops = transition[1]
            for i in range(pops):
                states.pop()
        # switch mode.
        elif isinstance(transition[1], str):
            states[-1] = transition[1]

        states += transition[0]
        
        transition_transform = transition[2]
        while transition_transform is not None:
            op, caller, extra_states = transition[2],
            extra_state_data = [
                    (states[-1 - i] if i <= len(states) else None) for i in extra_states 
            ] if extra_states is not None else []
            
            res = None
            if op == "generate":
                res = caller(token, *extra_state_data)
            elif op == "pop":
                end = results.pop()
                res = caller(token, *extra_state_data, end)
            elif op == "merge":
                end = results.pop()
                one_before_end = results.pop()
                res = caller(token, *extra_state_data, one_before_end, end)
            elif op == "merge_pair":
                end = results.pop()
                one_before_end = results.pop()
                two_before_end = results.pop()
                res = caller(token, *extra_state_data, two_before_end, one_before_end, end)
            elif op == "dyn":
                transition_transform = caller(token, *extra_state_data)
            else:
                raise Exception("Invalid op on parsing state transition: {}".format(op))
            if res is not None and op != "dyn":
                results.append(res)
            # ensure termination for non-dynamic operators.
            if op != "dyn":
                transition_transform = None


    """
    Map of current state category to a set of expected type patterns.
    when you are in a given state this maps that state to a transition table.
    the transition table is a map of token categories to transition as specified
    further above.

    If there is no transition in a given state for a given token category, this is
    an error.

    There's also a special token called "END" for reaching the end of the token stream.

    There's another special token type called "IST" (I(mmediate) S(tate) T(ransition))
    which if present will be immediately performed when the state is first entered/added
    to the end of the state stack. 

    Good for states which primarily exist for adding stuff "after" when first immediately
    transitioning into a different constructor nya
    """
    # some simple operations that enable stuff
    new_list = op("generate", lambda token: [])
    new_tuple = op("generate", lambda token: [])
    # maps
    new_map = op("generate", lambda token: {})

    # using the format of lists/tuples use lists and glue the contained data one after
    # in the result stack
    # and mappings glue the result name first then the result value, which is an argparam
    merge_list_elem = op("merge", lambda token, p_list, param_ast: p_list + [param_ast])
    merge_tuple_elem = op("merge", lambda token, p_tuplelist, param_ast: p_tuplelist + [param_ast])
    merge_mapping_elem = op("merge_pair", lambda token, p_mapping, name_str, param_ast: 
            merge_dictionaries(mapping + {name_str: param_ast})
    )


    # terminate mappings, lists, tuples one level above the currently-being-built thing,
    # ensuring that there actually is one one level above when the token is encountered.
    # these cause the token to be reparsed so the state parser for those can
    # process related things :)
    # this merges the last one into the given data structure being constructed one
    # above in the results/AST tree-stack
    append_to_terminate_containing_list = state_transition([], 1, pattern_op([
        (state_stack_pattern({"building_list"}), merge_list_elem)
    ]), True)

    append_to_terminate_containing_tuple = state_transition([], 1, pattern_op([
        (state_stack_pattern({"building_tuple"}), merge_tuple_elem)
    ]), True)

    append_to_terminate_containing_map = state_transition([], 1, pattern_op([
        (state_stack_pattern({"building_mapping"}), merge_mapping_elem)
    ]), True)

    tables = {
        "new_provided_argument": {
            "[": state_transition(["building_parameter", "building_list"], None, new_list),
            "(": state_transition(["building_argument", "building_tuple"]),
            "{": state_transition(["building_argument", "building_mapping"]),
            "reference": state_transition(["building_argument", "building_reference"]),
            "name": state_transition(["building_parameter", "building_name_parameter"], 0, None, True),
            "END": state_transition([])
        },
        "building_name_parameter": {
            "name": state_transition([], 1, ("generate", lambda token: ArgParam(token[1])))
        },
        "maybe_building_parameter": {  # if a parameter is detected it builds it, if not it jumps back.
            "name": state_transition(["building_name_parameter"], "building_parameter", None, True)
            "[": state_transition(["building_list"], "building_parameter", None, False),
            "]": state_transition([], 1, True)
        },
        "building_parameter": {
            '[': state_transition(["building_index"]),
            # ends the parameter and tells the system to rescan :) 
            'statement': state_transition([], 1, None, True)
            # Any collection terminator that happens without a comma needs to jump along.
            ']': append_to_terminate_containing_list
        },
        "building_index": {
            'int': state_transition([], 0, ("pop", lambda token, param_ast: ArgIndexedParam(param_ast, token[1]))),
            ']': state_transition([], 1)        
        },
        "building_list": {
            'IST': state_transition(["maybe_building_parameter"], 0, new_list),
            'statement': state_transition(["maybe_building_parameter"], 0), 
            ']': state_transition([], 1, ("pop", lambda token, ast_list: MultiParamList(ast_list)))
        },
        "": {

        }
    }

        
            


def parse_command_args(command_arguments: str) -> List[Arg]:
    """
    Convert a list of command arguments into a list of Arg instances (probably mostly argparams)
    that can then be used for parsing the environment out ^.^
    """
    # tokenise things ^.^
    tokens = tokenise_argstring(command_arguments)





class Command(object):
    """
    Holds a command data chunk
    """
    def __init__(self, command: str, text_block: str, args):
        """
        
        """


             
class Template(object):
    """
    Holds a template for html.
    """
    def __init__(self, text):
        pass




# <name> : <template> mapping
template_registry = {}
